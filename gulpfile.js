var gulp = require('gulp'),
less = require('gulp-less'),
minify = require('gulp-minify-css'),
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
rename = require('gulp-rename'),
notify = require('gulp-notify');
autoprefixer = require('gulp-autoprefixer');
livereload = require('gulp-livereload');

//Paths
var paths = {
    'dev': {
        'less': './resources/assets/less/',
        'js': './resources/assets/js/',
        'node_modules': './node_modules/'
    },
    'production': {
        'css': './public/css/',
        'js': './public/js/'
    },
};

// CSS
gulp.task('css', function() {
    return gulp.src(paths.dev.less + '*.less')
    .pipe(less())
    .pipe(autoprefixer())
    .pipe(gulp.dest(paths.production.css))
    .pipe(minify({keepSpecialComments: 0}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(paths.production.css))
    .pipe(livereload());
});

// JS
gulp.task('js', function() {
    return gulp.src([
        paths.dev.node_modules + 'jquery/dist/jquery.js',
        paths.dev.node_modules + 'bootstrap/dist/js/bootstrap.js',
        paths.dev.node_modules + 'swiper/dist/js/swiper.jquery.min.js',
        paths.dev.js + '*.js'
    ])
    .pipe(concat('all.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.production.js))
    .pipe(livereload());
});

gulp.task('html', function() {
    return gulp.src('./public/*.html')
    .pipe(livereload());
});

//Watch
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(paths.dev.less + '/*.less', ['css']);
    gulp.watch(paths.dev.js + '/*.js', ['js']);
    gulp.watch('./public/*.html', ['html']);
});

//Default
gulp.task('default', ['css', 'js', 'watch', 'html']);
