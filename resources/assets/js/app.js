console.log('App js loaded');

/**
* OnePageNav
*/
$('a.scroll-to').on('click', function(event) {
    $('html, body').stop().animate({
        scrollTop: $($(this).attr('href')).offset().top - 66
    }, 750);
    event.preventDefault();
});

/**
* AffixMenu
*/
$('#affix').affix({
    offset: {
        top: $('header').height() - 80
    }
});

/**
* closeMobileMenuWhenClicked
*/
$(function(){
    var menu = $(".navbar-collapse");

    menu.on("click", "a:not([data-toggle])", null, function () {
        menu.collapse('hide');
    });
});

/**
* divisionBoxAnimation
*/
(function($) {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

    } else {
        $('.info').each(function(){
            var infoHeight = $(this).children('.hide-info').height();
            $(this).css('bottom', (-infoHeight + 4) + 'px')
        });
    }
})(jQuery);

$('.division-box').on('mouseover', function(event){
    $(this).find('.info').css('bottom', '36px');
    event.preventDefault();
});

$('.division-box').on('mouseout', function(event){
    var infoHeight = $(this).find('.hide-info').height();
    $(this).find('.info').css('bottom', (-infoHeight + 4) + 'px')
    event.preventDefault();
});

/**
* Swiper
*/
var divisions = new Swiper('#divisions-swiper', {
    speed: 400,
    loop: true,
    spaceBetween: 15,
    slidesPerView: 1,
    nextButton: null,
    prevButton: null,
    pagination: '.swiper-pagination',
    paginationClickable: true
});

var project = new Swiper('#projects-swiper', {
    speed: 400,
    loop: true,
    spaceBetween: 30,
    slidesPerView: 1,
    nextButton: null,
    prevButton: null,
    pagination: '.swiper-pagination',
    paginationClickable: true
});

var chosen = new Swiper('#chosen-swiper', {
    speed: 400,
    loop: true,
    spaceBetween: 30,
    slidesPerView: 5,
    nextButton: null,
    prevButton: null,
    autoplay: 2000,
    // Responsive breakpoints
    breakpoints: {
        // when window width is <= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 30,
        },
        // when window width is <= 480px
        480: {
            slidesPerView: 1,
            spaceBetween: 30,
        },
        // when window width is <= 640px
        640: {
            slidesPerView: 5,
            spaceBetween: 30
        }
    }
});
